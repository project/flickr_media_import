CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------
The Flickr Media Import module is an integration for browsing and importing
photos from a flickr user account. It uses the public Flickr API without
needing to authenticate.

Flickr Media Import was written by Mark Millford.


REQUIREMENTS
------------
This module requires media and the flickr_api module.


INSTALLATION
------------
Install the Flickr Media Import as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further information.

1) Copy the flickr_media_import folder in the modules folder in your Drupal directory.

2) Enable the module using Manage -> Extend (/admin/modules).


CONFIGURATION
-------------
Before using the module, you will need to set the module configuration. This
includes the flickr user id (NSID) for the account that will be searched.
Additionally, the media bundle used for importing will need to be selected.
These settings are available at Manage -> Configuration -> Services -> Flickr
(/admin/config/services/flickr).
