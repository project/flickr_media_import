<?php

namespace Drupal\flickr_media_import\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\flickr_media_import\Form\FlickrGalleryFilterForm;
use Drupal\flickr_media_import\Form\FlickrGalleryImportForm;
use Drupal\flickr_media_import\Form\FlickrImportForm;

class LibraryController extends ControllerBase {

  public function gallery() {
    return [
      '#theme' => 'flickr_media_import_gallery',
      '#filters' => \Drupal::formBuilder()->getForm(FlickrGalleryFilterForm::class),
      '#photos' => \Drupal::formBuilder()->getForm(FlickrGalleryImportForm::class),
      '#pager' => [
        '#type' => 'pager',
      ],
    ];
  }

  public function import($id) {
    return \Drupal::formBuilder()->getForm(FlickrImportForm::class, $id);
  }
}
