<?php

namespace Drupal\flickr_media_import\Form;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Pager\PagerManager;
use Drupal\Core\Url;
use Drupal\flickr_api\Service\Photos;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;
use Drupal\media\Plugin\media\Source\Image;

class FlickrGalleryImportForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'flickr_media_import_gallery_import';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = \Drupal::config('flickr_media_import.settings');
    $request = \Drupal::request();

    /** @var PagerManager $pager */
    $pager = \Drupal::service('pager.manager');

    /** @var Photos $photos_api */
    $photos_api = \Drupal::service('flickr_api.photos');

    $args = [
      'per_page' => $request->get('perpage', 25),
      'sort' => $request->get('sort', 'relevance'),
      'media' => 'photos',
    ];

    if ($request->get('text')) {
      $args['text'] = $request->get('text');
    }

    $photos = $photos_api->photosSearch($config->get('nsid'), $pager->findPage() + 1, $args);

    if (!$photos) {
      $form['error'] = [
        '#markup' => $this->t('An error occurred fetching the photos.'),
      ];
    }
    else {
      $pager->createPager($photos['total'], $photos['perpage']);

      $form['gallery'] = [
        '#type' => 'tableselect',
        '#header' => [
          'title' => $this->t('Title'),
          'photo' => $this->t('Photo'),
          'operations' => $this->t('Operations'),
        ],
        '#options' => [],
        '#empty' => $this->t('No matching photos were found'),
      ];

      foreach ($photos['photo'] as $photo) {
        $form['gallery']['#options'][$photo['id']] = [
          'title' => $photo['title'],
          'photo' => ['data' => [
            '#theme' => 'flickr_media_import_photo',
            '#photo' => $photo,
            '#size' => $config->get('preview_size'),
          ]],
          'operations' => ['data' => [
            '#type' => 'operations',
            '#links' => [
              'details' => [
                'title' => $this->t('Details'),
                'url' => Url::fromRoute('flickr_media_import.import', [
                  'id' => $photo['id'],
                ]),
              ],
            ],
          ]],
        ];
      }

      $form['actions'] = [
        '#type' => 'actions',
      ];

      $form['actions']['import'] = [
        '#type' => 'submit',
        '#value' => $this->t('Import'),
      ];
    }

    $form['#cache']['max-age'] = 0;

    return $form;
  }

  /**
   * Form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $photos = array_filter($form_state->getValue('gallery'));

    if (empty($photos)) {

    }
    elseif (count($photos) === 1) {

    }
    else {
      /** @var ModuleExtensionList $module_extension_list */
      $module_extension_list = \Drupal::service('extension.list.module');

      $batch = [
        'operations' => [],
        'title' => $this->t('Importing'),
        'finished' => 'flickr_media_import_batch_finished',
        'file' => $module_extension_list->getPath('flickr_media_import') . '/flickr_media_import.batch.inc',
      ];

      foreach ($photos as $photo_id) {
        $batch['operations'][] = [
          'flickr_media_import_batch_run',
          [$photo_id],
        ];
      }

      batch_set($batch);
    }
  }
}
