<?php

namespace Drupal\flickr_media_import\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\flickr_api\Service\Photos;
use Drupal\flickr_media_import\FlickrMediaImportService;
use Drupal\media_library\MediaLibraryState;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FlickrGalleryModalForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'flickr_media_import_modal_import';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MediaLibraryState $state = NULL) {

    $config = \Drupal::config('flickr_media_import.settings');

    $form['state']['media_library_opener_id'] =
    $form['state']['media_library_allowed_types'] =
    $form['state']['media_library_selected_type'] =
    $form['state']['media_library_remaining'] =
    $form['state']['media_library_opener_parameters'] =
    $form['state']['media_library_hash'] = [
      '#type' => 'hidden',
    ];

    if ($state) {
      $form['state']['media_library_opener_id']['#value'] = $state->get('media_library_opener_id');
      $form['state']['media_library_allowed_types']['#value'] = serialize($state->all('media_library_allowed_types'));
      $form['state']['media_library_selected_type']['#value'] = $state->get('media_library_selected_type');
      $form['state']['media_library_remaining']['#value'] = $state->get('media_library_remaining');
      $form['state']['media_library_opener_parameters']['#value'] = serialize($state->all('media_library_opener_parameters'));
      $form['state']['media_library_hash']['#value'] = $state->getHash();
    }

    $ajax = [
      '#ajax' => [
        'callback' => [static::class, 'ajaxCallback'],
        'wrapper' => 'flickr-modal-content',
        'url' => Url::fromRoute('flickr_media_import.modal_ajax'),
        'options' => [
          'query' => [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
      ],
    ];

    $form['search'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline'],
      ],
    ];

    $form['search']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#default_value' => $form_state->getValue('text'),
      '#description' => $this->t('Photos whose title, description or tags contain the text will be returned. You can exclude results that match a term by prepending it with a - character.'),
    ];

    $form['search']['perpage'] = [
      '#type' => 'select',
      '#title' => $this->t('Per Page'),
      '#default_value' => $form_state->getValue('perpage', 25),
      '#options' => [
        10 => 10,
        25 => 25,
        50 => 50,
        100 => 100,
      ],
    ];

    $form['search']['sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort'),
      '#default_value' => $form_state->getValue('sort', 'relevance'),
      '#options' => [
        'date-posted-asc' => $this->t('Date Posted (Asc)'),
        'date-posted-desc' => $this->t('Date Posted (Desc)'),
        'date-taken-asc' => $this->t('Date Taken (Asc)'),
        'date-taken-desc' => $this->t('Date Taken (Desc)'),
        'interestingness-desc' => $this->t('Interestingness (Desc)'),
        'interestingness-asc' => $this->t('Interestingness (Asc)'),
        'relevance' => $this->t('Relevance'),
      ],
    ];

    $form['search']['search'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#validate' => [],
      '#submit' => [[static::class, 'search']],
    ] + $ajax;

    $form['page'] = [
      '#type' => 'hidden',
      '#value' => $form_state->getValue('page', 1),
    ];

    $args = [
      'per_page' => $form['search']['perpage']['#default_value'],
      'sort' => $form['search']['sort']['#default_value'],
      'media' => 'photos',
    ];

    if ($form['search']['text']['#default_value']) {
      $args['text'] = $form['search']['text']['#default_value'];
    }

    /** @var Photos $photos_api */
    $photos_api = \Drupal::service('flickr_api.photos');
    $photos = $photos_api->photosSearch($config->get('nsid'), $form['page']['#value'], $args);

    if (!$photos) {
      $form['error'] = [
        '#markup' => $this->t('An error occurred fetching the photos.'),
      ];
    }
    else {
      $form['gallery'] = [
        '#type' => 'tableselect',
        '#header' => [
          'title' => $this->t('Title'),
          'photo' => $this->t('Photo'),
          'operations' => $this->t('Operations'),
        ],
        '#options' => [],
        '#empty' => $this->t('No matching photos were found'),
        // TODO : see about supporting multiple in the future.
        '#multiple' => FALSE,
      ];

      foreach ($photos['photo'] as $photo) {
        $form['gallery']['#options'][$photo['id']] = [
          'title' => $photo['title'],
          'photo' => ['data' => [
            '#theme' => 'flickr_media_import_photo',
            '#photo' => $photo,
            '#size' => $config->get('preview_size'),
          ]],
          'operations' => ['data' => [
            '#type' => 'operations',
            '#links' => [
              'details' => [
                'title' => $this->t('Details'),
                'url' => Url::fromRoute('flickr_media_import.import', [
                  'id' => $photo['id'],
                ]),
              ],
            ],
          ]],
        ];
      }

      $form['actions'] = [
        '#type' => 'actions',
      ];

      $form['actions']['import'] = [
        '#type' => 'submit',
        '#value' => $this->t('Import'),
        '#validate' => [[static::class, 'validateMediaLibrary']],
        '#submit' => [[static::class, 'import']],
        '#button_type' => 'primary',
      ] + $ajax;

      // Different ajax callback for the primary action.
      $form['actions']['import']['#ajax']['callback'] = [static::class, 'importCallback'];

      $form['actions']['prev'] = [
        '#type' => 'submit',
        '#value' => $this->t('Previous Page'),
        '#validate' => [],
        '#submit' => [[static::class, 'prevPage']],
      ] + $ajax;

      if ($form['page']['#value'] <= 1) {
        $form['actions']['prev']['#disabled'] = TRUE;
      }

      $form['actions']['next'] = [
        '#type' => 'submit',
        '#value' => $this->t('Next Page'),
        '#validate' => [],
        '#submit' => [[static::class, 'nextPage']],
      ] + $ajax;

      if ($form['page']['#value'] >= $photos['pages']) {
        $form['actions']['next']['#disabled'] = TRUE;
      }
    }

    return $form;
  }

  public static function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'flickr-modal-content',
      ],
      'form' => $form,
    ];
  }

  /**
   * Form submit callback for page navigation.
   */
  public static function search(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('page', 1);
    $form_state->setRebuild();
  }

  /**
   * Form submit callback for page navigation.
   */
  public static function prevPage(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('page', $form_state->getValue('page') - 1);
    $form_state->setRebuild();
  }

  /**
   * Form submit callback for page navigation.
   */
  public static function nextPage(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('page', $form_state->getValue('page') + 1);
    $form_state->setRebuild();
  }

  public static function import(array &$form, FormStateInterface $form_state) {
    $selected = array_filter((array)$form_state->getValue('gallery', []));
    $media = [];
    foreach ($selected as $flickr_id) {
      // TODO : batch download? check if that works in an ajax modal form.
      /** @var FlickrMediaImportService $importer */
      $importer = \Drupal::service('flickr_media_import.importer');
      $entity = $importer->get($flickr_id);
      $media[] = $entity->id();
    }
    $form_state->set('media', $media);
  }

  public static function validateMediaLibrary(array &$form, FormStateInterface $form_state) {

    // Validate that the media library state information came through correctly.
    $state = MediaLibraryState::create(
      $form_state->getValue('media_library_opener_id'),
      unserialize($form_state->getValue('media_library_allowed_types')),
      $form_state->getValue('media_library_selected_type'),
      $form_state->getValue('media_library_remaining'),
      unserialize($form_state->getValue('media_library_opener_parameters')),
    );

    // The request parameters need to contain a valid hash to prevent a
    // malicious user modifying the query string to attempt to access
    // inaccessible information.
    if (!$state->isValidHash($form_state->getValue('media_library_hash'))) {
      $form_state->setError($form, "Invalid media library parameters specified.");
    }
    else {
      $form_state->set('media_library_state', $state);
    }
  }

  public static function importCallback(array &$form, FormStateInterface $form_state) {

    if ($form_state->getErrors()) {
      // If there are any errors, just rebuild the form.
      return static::ajaxCallback($form, $form_state);
    }

    $ids = $form_state->has('media') ? $form_state->get('media') : [];

    $state = $form_state->get('media_library_state');
    $parameters = $state->getOpenerParameters();
    $widget_id = $parameters['field_widget_id'];

    return (new AjaxResponse())
      ->addCommand(new CloseDialogCommand())
      ->addCommand(new InvokeCommand("[data-media-library-widget-value=\"$widget_id\"]", 'val', [$ids]))
      ->addCommand(new InvokeCommand("[data-media-library-widget-update=\"$widget_id\"]", 'trigger', ['mousedown']));
  }

  /**
   * Form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }
}
