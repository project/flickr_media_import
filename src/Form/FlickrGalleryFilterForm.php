<?php

namespace Drupal\flickr_media_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Pager\PagerManager;
use Drupal\flickr_api\Service\Galleries;
use Drupal\flickr_api\Service\Groups;
use Drupal\flickr_api\Service\Helpers;
use Drupal\flickr_api\Service\People;
use Drupal\flickr_api\Service\Photos;
use Drupal\flickr_api\Service\Photosets;
use Drupal\flickr_api\Service\Tags;

class FlickrGalleryFilterForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'flickr_media_import_gallery_filter';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = \Drupal::config('flickr_media_import.settings');

    if (!$config->get('nsid')) {
      $form['error'] = Link::createFromRoute($this->t('Configuration Required'), 'flickr_media_import.settings')->toRenderable();
      return $form;
    }

    $request = \Drupal::request();

    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#default_value' => $request->get('text'),
      '#description' => $this->t('Photos whose title, description or tags contain the text will be returned. You can exclude results that match a term by prepending it with a - character.'),
    ];

    $form['perpage'] = [
      '#type' => 'select',
      '#title' => $this->t('Per Page'),
      '#default_value' => $request->get('perpage', 25),
      '#options' => [
        10 => 10,
        25 => 25,
        50 => 50,
        100 => 100,
      ],
    ];

    $form['sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort'),
      '#default_value' => $request->get('sort', 'relevance'),
      '#options' => [
        'date-posted-asc' => $this->t('Date Posted (Asc)'),
        'date-posted-desc' => $this->t('Date Posted (Desc)'),
        'date-taken-asc' => $this->t('Date Taken (Asc)'),
        'date-taken-desc' => $this->t('Date Taken (Desc)'),
        'interestingness-desc' => $this->t('Interestingness (Desc)'),
        'interestingness-asc' => $this->t('Interestingness (Asc)'),
        'relevance' => $this->t('Relevance'),
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['search'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    // Prevent form caching and set form to use GET.
    $form['#cache']['max-age'] = 0;
    $form_state->setMethod('GET');
    $form['#after_build'][] = [get_class($this), 'afterBuild'];

    // Layout classes.
    $form['#attributes']['class'][] = 'form--inline';
    $form['#attributes']['class'][] = 'clearfix';

    return $form;
  }

  /**
   * Remove form processing elements that aren't needed for a GET request.
   */
  public static function afterBuild(array $form, FormStateInterface $form_state) {
    unset($form['form_token']);
    unset($form['form_build_id']);
    unset($form['form_id']);
    return $form;
  }

  /**
   * Form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Redirect to the same page with submitted values as query args.
    $form_state->setRedirect('<current>', $form_state->getValues());
  }
}
