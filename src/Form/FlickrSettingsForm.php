<?php

namespace Drupal\flickr_media_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\flickr_api\Service\Helpers;
use Drupal\media\Entity\MediaType;
use Drupal\media\Plugin\media\Source\Image;

class FlickrSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'flickr_media_import_settings';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['flickr_media_import.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('flickr_media_import.settings');

    $flickr_api_config = $this->config('flickr_api.settings');
    $apiKey = $flickr_api_config->get('api_key');
    $apiSecret = $flickr_api_config->get('api_secret');

    /** @var Helpers $helpers */
    $helpers = \Drupal::service('flickr_api.helpers');
    $photo_sizes = $helpers->photoSizes();

    if (empty($apiKey) || empty($apiSecret)) {
      $msg = $this->t('Flickr API credentials are not set. It can be set on the <a href=":config_page">configuration page</a>.',
        [':config_page' => Url::fromRoute('flickr_api.settings')->toString()]
      );

      \Drupal::messenger()->addError($msg);
    }

    $form['nsid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Flickr User NSID'),
      '#description' => $this->t('The NSID of the Flickr user account whose images to display.'),
      '#default_value' => $config->get('nsid'),
    ];

    $form['preview_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Gallery Preview Size'),
      '#description' => $this->t('The preview size used for the gallery previews.'),
      '#default_value' => $config->get('preview_size'),
      '#options' => [],
    ];

    // The larger sizes are not included as options for the gallery.
    foreach (['s', 'q', 't', 'm', 'n', '-', 'z', 'c', 'b'] as $key) {
      if (isset ($photo_sizes[$key])) {
        $form['preview_size']['#options'][$key] = $photo_sizes[$key]['label'];
      }
    }

    $form['download_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Download Size'),
      '#description' => $this->t('The size when importing images.'),
      '#default_value' => $config->get('download_size') ?: 'o',
      '#options' => [],
    ];

    // Exclude the slideshow options, since they don't apply here.
    foreach (['s', 't', 'q', 'm', 'n', 'w', '-', 'z', 'c', 'b', 'h', 'k', 'o'] as $key) {
      if (isset ($photo_sizes[$key])) {
        $form['download_size']['#options'][$key] = $photo_sizes[$key]['label'];
      }
    }

    $form['import_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Import Media Type'),
      '#options' => [],
      '#default_value' => $config->get('import_type'),
    ];

    /** @var MediaType[] $media_bundles */
    $media_bundles = \Drupal::entityTypeManager()->getStorage('media_type')->loadMultiple();
    foreach ($media_bundles as $bundle_id => $media_bundle) {
      if ($media_bundle->getSource() instanceof Image) {
        $form['import_type']['#options'][$bundle_id] = $media_bundle->label();
      }
    }

    if ($form['import_type']['#default_value'] && !isset($form['import_type']['#options'][$form['import_type']['#default_value']])) {
      $form['import_type']['#default_value'] = NULL;
    }

    if ($config->get('import_type')) {
      $form['import_tags'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Import Tags'),
        '#default_value' => $config->get('import_tags'),
      ];

      $form['tags_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Tags Field'),
        '#options' => [],
        '#default_value' => $config->get('tags_field'),
        '#description' => $this->t('A taxonomy reference field on the selected media bundle where tags will be imported.'),
        '#states' => [
          'required' => [
            ':input[name="import_tags"]' => ['checked' => TRUE],
          ],
          'visible' => [
            ':input[name="import_tags"]' => ['checked' => TRUE],
          ],
        ],
      ];

      /** @var FieldConfig[] $fields */
      $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('media', $config->get('import_type'));

      foreach ($fields as $field_name => $field) {
        if ($field instanceof FieldConfig && $field->getFieldStorageDefinition()->getType() === 'entity_reference' && $field->getFieldStorageDefinition()->getSettings()['target_type'] === 'taxonomy_term') {
          $form['tags_field']['#options'][$field_name] = $field->label();
        }
      }

      if ($form['tags_field']['#default_value'] && !isset($form['tags_field']['#options'][$form['tags_field']['#default_value']])) {
        $form['tags_field']['#default_value'] = NULL;
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('flickr_media_import.settings')
      ->set('nsid', $form_state->getValue('nsid'))
      ->set('preview_size', $form_state->getValue('preview_size'))
      ->set('download_size', $form_state->getValue('download_size'))
      ->set('import_type', $form_state->getValue('import_type'))
      ->set('import_tags', $form_state->getValue('import_tags'))
      ->set('tags_field', $form_state->getValue('tags_field'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
