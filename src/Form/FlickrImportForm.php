<?php

namespace Drupal\flickr_media_import\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\flickr_api\Service\Photos;
use Drupal\flickr_media_import\FlickrMediaImportService;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;
use Drupal\media\Plugin\media\Source\Image;

class FlickrImportForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'flickr_media_import_import';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    if (!\Drupal::config('flickr_media_import.settings')->get('import_type')) {
      $form['error'] = Link::createFromRoute($this->t('Configuration Required'), 'flickr_media_import.settings')->toRenderable();
      return $form;
    }

    $args = $form_state->getBuildInfo()['args'];

    $id = reset($args);

    /** @var Photos $photos_api */
    $photos_api = \Drupal::service('flickr_api.photos');
    $photo = $photos_api->photosGetInfo($id);

    $form['data'] = [
      '#theme' => 'table',
      '#rows' => [[
        $this->t('Title'),
        $photo['title']['_content'],
      ], [
        $this->t('Description'),
        $photo['description']['_content'],
      ], [
        $this->t('Date Taken'),
        $photo['dates']['taken'],
      ], [
        $this->t('Date Uploaded'),
        date('m/d/Y H:i:s', $photo['dateuploaded']),
      ], [
        $this->t('Date Posted'),
        date('m/d/Y H:i:s', $photo['dates']['posted']),
      ], [
        $this->t('Date Updated'),
        date('m/d/Y H:i:s', $photo['dates']['lastupdate']),
      ], [
        $this->t('Tags'),
        htmlentities(join(', ', array_column($photo['tags']['tag'], 'raw'))),
      ], [
        $this->t('Country'),
        $photo['location']['country']['_content'],
      ], [
        $this->t('Region'),
        $photo['location']['region']['_content'],
      ], [
        $this->t('County'),
        $photo['location']['county']['_content'],
      ], [
        $this->t('Locality'),
        $photo['location']['locality']['_content'],
      ], [
        $this->t('Neighborhood'),
        $photo['location']['neighbourhood']['_content'],
      ]],
    ];

    $form['photo'] = [
      '#theme' => 'flickr_media_import_photo',
      '#photo' => $photo,
      '#size' => 'o',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    $form['#cache']['max-age'] = 0;

    return $form;
  }

  /**
   * Form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Fetch the passed image id.
    $args = $form_state->getBuildInfo()['args'];
    $id = reset($args);

    /** @var FlickrMediaImportService $importer */
    $importer = \Drupal::service('flickr_media_import.importer');
    $media = $importer->get($id);

    // Redirect to the edit form for the newly created media.
    $form_state->setRedirect('entity.media.edit_form', [
      'media' => $media->id(),
    ]);
  }
}
