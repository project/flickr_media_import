<?php

namespace Drupal\flickr_media_import;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\flickr_api\Service\Photos;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;
use Drupal\media\Plugin\media\Source\Image;

class FlickrMediaImportService {

  /** @var Photos */
  protected $photos_api;

  /** @var Connection */
  protected $db;

  /** @var ImmutableConfig */
  protected $config;

  /** @var AccountProxyInterface */
  protected $current_user;

  /** @var EntityTypeManagerInterface */
  protected $entity_type_manager;

  /** @var ModuleHandlerInterface */
  protected $module_handler;

  /**
   * @param Photos $photos_api
   * @param Connection $database
   * @param ConfigFactoryInterface $config
   * @param AccountProxyInterface $current_user
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(
    Photos $photos_api,
    Connection $database,
    ConfigFactoryInterface $config,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler
  ) {
    $this->photos_api = $photos_api;
    $this->db = $database;
    $this->config = $config->get('flickr_media_import.settings');
    $this->current_user = $current_user;
    $this->entity_type_manager = $entity_type_manager;
    $this->module_handler = $module_handler;
  }

  /**
   * Return a media entity representing this image, creating one if necessary.
   *
   * @param $flickr_photo_id
   * @return Media|null
   */
  public function get($flickr_photo_id) {

    $media = $this->check($flickr_photo_id);

    if (!$media) {
      $media = $this->download($flickr_photo_id);
    }

    return $media;
  }

  /**
   * Download a flickr photo and create a meida item with it.
   *
   * @param $flickr_photo_id
   * @return Media
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function download($flickr_photo_id) {
    // Build the url for the original source image.
    $photo = $this->photos_api->photosGetInfo($flickr_photo_id);

    // Fetch the image field name from the media bundle.
    $size = $this->config->get('download_size');

    $url = flickr_media_import_url($photo, $size ?: 'o');

    // Fetch the image field name from the media bundle.
    $media_bundle_id = $this->config->get('import_type');
    /** @var MediaType $media_bundle */
    $media_bundle = $this->entity_type_manager->getStorage('media_type')->load($media_bundle_id);
    /** @var Image $source */
    $source = $media_bundle->getSource();
    $field = $source->getConfiguration()['source_field'];

    // Download the photo from flickr.
    $file = system_retrieve_file($url, 'public://', TRUE, FileSystemInterface::EXISTS_RENAME);

    if (!$file) {
      // TODO : download error handling
    }

    // Create the media entity.
    $media = Media::create([
      'bundle' => $media_bundle_id,
      'uid' => $this->current_user->id(),
      $field => [
        'target_id' => $file->id(),
        // Trim any long descriptions down to the maximum length for the alt field in the image widget.
        'alt' => substr($photo['description']['_content'], 0, 512),
      ],
    ]);

    $media->setName($photo['title']['_content'])
      ->setPublished(TRUE);

    $this->module_handler->invokeAll('flickr_media_import', [
      $media,
      $photo,
      $this->config,
    ]);

    $media->save();

    // Remove any previous (and presumably deleted) imports for this photo.
    $this->db->delete('flickr_media_import')
      ->condition('photo_id', $flickr_photo_id)
      ->execute();

    // Record this media id to prevent duplicate imports later.
    $this->db->insert('flickr_media_import')
      ->fields([
        'mid' => $media->id(),
        'photo_id' => $flickr_photo_id,
      ])
      ->execute();

    return $media;
  }

  /**
   * Return the media item corresponding to a given flickr photo, if one already exists.
   *
   * @param $flickr_photo_id
   * @return Media|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function check($flickr_photo_id) {

    if (empty($flickr_photo_id)) {
      return NULL;
    }

    $mid = $this->db->select('flickr_media_import')
      ->fields('flickr_media_import', ['mid'])
      ->condition('photo_id', $flickr_photo_id)
      ->execute()
      ->fetchField();

    if ($mid) {
      /** @var Media $media */
      $media = $this->entity_type_manager->getStorage('media')->load($mid);
      if ($media) {
        return $media;
      }
    }

    return NULL;
  }

}
