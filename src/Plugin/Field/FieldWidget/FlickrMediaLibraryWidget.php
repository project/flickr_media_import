<?php

namespace Drupal\flickr_media_import\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\flickr_media_import\Form\FlickrGalleryModalForm;
use Drupal\media_library\Plugin\Field\FieldWidget\MediaLibraryWidget;

/**
 * Add Flickr support to existing Media Library widget.
 *
 * @FieldWidget(
 *   id = "flickr_media_library_widget",
 *   label = @Translation("Flickr / Media library"),
 *   description = @Translation("Allows you to select items from either flickr or the media library."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE,
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
class FlickrMediaLibraryWidget extends MediaLibraryWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (isset($element['open_button'])) {
      $field_name = $this->fieldDefinition->getName();
      $parents = $form['#parents'];
      $id_suffix = $parents ? '-' . implode('-', $parents) : '';

      $element['flickr_button'] = [
        '#type' => 'button',
        '#value' => $this->t('Add from Flickr'),
        '#name' => $field_name . '-flickr-button' . $id_suffix,
        '#attributes' => [
          'class' => [
            'js-flickr-open-button',
          ],
        ],
        '#media_library_state' => $element['open_button']['#media_library_state'],
        '#ajax' => [
          'callback' => [static::class, 'openFlickr'],
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Opening Flickr.'),
          ],
          'disable-refocus' => TRUE,
        ],
        '#limit_validation_errors' => [],
      ];

      if (isset($element['open_button']['#disabled'])) {
        $element['flickr_button']['#disabled'] = $element['open_button']['#disabled'];
      }

      if (isset($element['open_button']['#attributes']['data-disabled-focus'])) {
        $element['flickr_button']['#attributes']['data-disabled-focus'] = $element['open_button']['#attributes']['data-disabled-focus'];
      }

      if (in_array('visually-hidden', $element['open_button']['#attributes']['class'])) {
        $element['flickr_button']['#attributes']['class'][] = 'visually-hidden';
      }
    }

    return $element;
  }

  /**
   * AJAX callback to open the library modal.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response to open the media library.
   */
  public static function openFlickr(array $form, FormStateInterface $form_state) {

    $triggering_element = $form_state->getTriggeringElement();

    $flickr_ui = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'flickr-modal-content',
      ],
      'form' => \Drupal::formBuilder()->getForm(FlickrGalleryModalForm::class, $triggering_element['#media_library_state']),
    ];

    $dialog_options = [
      'dialogClass' => 'flickr-widget-modal',
      'title' => t('Select Photo'),
      'height' => '75%',
      'width' => '75%',
    ];

    return (new AjaxResponse())
      ->addCommand(new OpenModalDialogCommand($dialog_options['title'], $flickr_ui, $dialog_options));
  }
}
