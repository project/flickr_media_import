<?php

use Drupal\Core\Link;
use Drupal\flickr_media_import\FlickrMediaImportService;

function flickr_media_import_batch_run($flickr_id, &$context) {
  /** @var FlickrMediaImportService $importer */
  $importer = \Drupal::service('flickr_media_import.importer');
  $media = $importer->get($flickr_id);
  $context['results'][] = t('Imported %media.', [
    '%media' => Link::fromTextAndUrl($media->label(), $media->toUrl('edit-form'))->toString(),
  ]);
}

function flickr_media_import_batch_finished($success, $results, $operations, $elapsed) {
  $messenger = \Drupal::messenger();
  foreach ($results as $message) {
    $messenger->addMessage($message);
  }
}
